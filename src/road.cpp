#include "road.h"

void Road::update() {
	for (const auto& dir : directionChancePercent) {
		if (dir.second > 0 && Random::instance().prob(dir.second)) {
			spawnCar(dir.first);
		}
	}

	for (auto& lane : lanes) {
		if (!lane.greenLight) {
			continue;
		}

		if (Random::instance().prob(carDespawnChance)) {
			despawnCar(lane);
			updateLanes();
		}
	}

	updateCarLifetimes();
}

void Road::spawnCar(Direction direction) {
	std::vector<Lane*> availableLanes{};
	for (auto& lane : lanes) {
		if (lane.availableDirections.at(direction)) {
			availableLanes.push_back(&lane);
		}
	}

	if (availableLanes.size() > 1) {
		std::sort(
			availableLanes.begin(),
			availableLanes.end(),
			[](Lane* a, Lane* b) {
				return a->cars.size() < b->cars.size();
			});
	}

	for (auto& lane : availableLanes) {
		if (!lane->capped) {
			lane->cars.emplace_back(Car(direction));
			return;
		}
		else {
			bool spawned = false;
			if (lane->left != nullptr) {
				if (tryEnteringFrom(*lane, *lane->left, direction)) {
					return;
				}
			}
			if (lane->right != nullptr) {
				if (tryEnteringFrom(*lane, *lane->right, direction)) {
					return;
				}
			}
		}
	}
	std::cout << "Couldn't find a spot!";
}

bool Road::tryEnteringFrom(Lane& lane, Lane& from, Direction direction) {
	if (!from.capped) {
		if (from.cars.size() < lane.capacity) {
			if (lane.cars.size() < lane.capacity) {
				lane.cars.emplace_back(Car(direction));
				return true;
			}
		}
		// No spot in desired lane? Time to fill up the other one with nullopts and place ourself in the entry point
		for (std::size_t i = from.cars.size(); i < lane.capacity; i++) {
			from.cars.emplace_back(std::nullopt);
		}
		from.cars.emplace_back(Car(direction));
		return true;
	}
	return false;
}

void Road::despawnCar(Lane& lane) {
	if (lane.cars.empty()) {
		return;
	}

	for (auto& car : lane.cars) {
		if (!car.has_value()) {
			continue;
		}
		if (lane.availableDirections[car.value().direction]) {
			despawnedCars += 1;
			totalLifetime += car.value().lifetime;
			car = std::nullopt;
			break;
		}
		else {
			break;
		}
	}
}

void Road::updateLanes() {
	for (auto& lane : lanes) {
		updateLane(lane);
		cleanLanes();
	}
}

void Road::updateLane(Lane& lane) {
	// Move all cars until hitting another
	for (int i = 1; i < lane.cars.size(); i++) {
		if (!lane.cars[i].has_value()) {
			continue;
		}

		// Not the lane we want to be in? Try to switch
		if (!lane.availableDirections[lane.cars[i].value().direction]) {
			Direction direction = lane.cars[i].value().direction;

			if (lane.left != nullptr && lane.left->availableDirections[direction]) {
				// Standing in a spot where we could enter? Try to move in
				if (i == lane.left->capacity && lane.left->cars.size() < lane.left->capacity) {
					lane.left->cars.push_back(lane.cars[i]);
					lane.cars[i] = std::nullopt;
					continue;
				}
				if (i == lane.left->capacity) {
					continue;
				}
			}
			if (lane.right != nullptr && lane.right->availableDirections[direction]) {
				if (i == lane.right->capacity && lane.right->cars.size() < lane.right->capacity) {
					lane.right->cars.push_back(lane.cars[i]);
					lane.cars[i] = std::nullopt;
					continue;
				}
				if (i == lane.right->capacity) {
					continue;
				}
			}
		}
		if (lane.cars[i - 1].has_value()) {
			break;
		}
		else {
			lane.cars[i - 1] = lane.cars[i];
			lane.cars[i] = std::nullopt;
		}

	}
}

void Road::createLanes(unsigned int number) {
	for (unsigned int i = 0; i < number; i++) {
		lanes.emplace_back(Lane());
	}
}

void Road::cleanLanes() {
	for (auto& lane : lanes) {
		if (lane.cars.size() == 0) {
			continue;
		}

		if (lane.cars.back() == std::nullopt) {
			lane.cars.pop_back();
		}
	}
}

void Road::updateCarLifetimes() {
	for (auto& lane : lanes) {
		for (auto& car : lane.cars) {
			if (car.has_value()) {
				car.value().lifetime += 1;
			}
		}
	}
}

std::size_t Road::laneToIndex(unsigned int lane) {
	constexpr unsigned int laneToIndexDifference = 1;
	return lane - laneToIndexDifference;
}

std::size_t Road::findShortestLane(std::vector<Lane> lanes) {
	std::size_t shortest = 0;
	for (std::size_t i = 0; i < lanes.size(); i++) {
		if (lanes[i].cars.size() < lanes[shortest].cars.size()) {
			shortest = i;
		}
	}
	return shortest;
}
