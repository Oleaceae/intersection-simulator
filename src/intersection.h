#pragma once

#include <vector>
#include <iostream>
#include <tuple>

#include "road.h"

struct Intersection {
	void simulate(unsigned int ticks);
	void simulateCycle();
	void printState();
	void updateLights(unsigned int second);
	bool isWithinInterval(unsigned int second, std::tuple<unsigned int, unsigned int> interval);
	void connectAllLanes();

	std::vector<Road> roads{};
	unsigned int secondsPerCycle{ 60 };
};
