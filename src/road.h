#pragma once

#include <vector>
#include <map>
#include <ranges>
#include <iostream>

#include "lane.h"
#include "car.h"
#include "random.h"

struct Road {
	void update();
	void spawnCar(Direction direction);
	bool tryEnteringFrom(Lane& lane, Lane& from, Direction direction);
	void despawnCar(Lane& lane);
	void updateLanes();
	void updateLane(Lane& lane);
	void createLanes(unsigned int number);
	void cleanLanes();
	void updateCarLifetimes();

	std::size_t laneToIndex(unsigned int lane);
	std::size_t findShortestLane(std::vector<Lane> lanes);
	std::size_t totalLifetime{};
	std::size_t despawnedCars{};

	std::vector<Lane> lanes{};
	std::map<Direction, unsigned int> directionChancePercent{};
	std::map<Direction, unsigned int> spawnLocation{};

	static constexpr unsigned int shortestLane = 0;
	static constexpr unsigned int carDespawnChance = 50;
};
