﻿#include "intersection.h"
#include "loader.h"

int main()
{
	constexpr unsigned int twoHours = 120;

	Intersection intersection = Loader::loadDefault();

	intersection.simulate(twoHours);

	return 0;
}
