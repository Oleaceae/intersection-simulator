#pragma once

enum class Direction {
	straight,
	left,
	right
};

struct Car {
	Direction direction{};
	std::size_t lifetime{};
};
