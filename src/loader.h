#pragma once

#include "intersection.h"

struct Loader {
	static Intersection loadDefault();

	static Road loadRoadA();
	static Road loadRoadB();
	static Road loadRoadC();
	static Road loadRoadD();
};
