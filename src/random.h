#pragma once

#include <random>

class Random {
public:
	static Random& instance() {
		static Random r;
		return r;
	}

	Random(const Random&) = delete;
	Random& operator=(const Random&) = delete;

	bool prob(int percent) {
		if (distribution(engine) <= percent) {
			return true;
		}
		else {
			return false;
		}
	}

private:
	Random() {
		engine = std::mt19937(seed());
	}
	~Random() {}

	static constexpr unsigned int min = 0;
	static constexpr unsigned int max = 100;

	std::random_device seed{};
	std::mt19937 engine{};
	std::uniform_int_distribution<> distribution{ min, max };
};
