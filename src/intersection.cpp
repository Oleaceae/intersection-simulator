#include "intersection.h"

void Intersection::simulate(unsigned int ticks) {
	for (unsigned int i = 0; i < ticks; i++) {
		simulateCycle();
	}
	printState();
}

void Intersection::simulateCycle() {
	for (unsigned int i = 0; i < secondsPerCycle; i++) {
		updateLights(i);
		for (auto& road : roads) {
			road.update();
		}
	}
}

void Intersection::printState() {
	std::string output{};
	for (const auto& road : roads) {
		output += std::format("Cars on road:");
		for (const auto& lane : road.lanes) {
			output += std::format(" {},", lane.cars.size());
		}
		if (road.despawnedCars > 0) {
			std::size_t carsAlive{};
			for (const auto& lane : road.lanes) {
				carsAlive += lane.cars.size();
			}
			output += std::format(" Total spawned/despawned: {}/{}, average lifetime: {}\n",
				carsAlive + road.despawnedCars,
				road.despawnedCars,
				static_cast<float>(road.totalLifetime) / road.despawnedCars);
		}
	}
	std::cout << output << std::endl;
}

void Intersection::updateLights(unsigned int second) {
	for (auto& road : roads) {
		for (auto& lane : road.lanes) {
			lane.greenLight = isWithinInterval(second, lane.greenInterval);
		}
	}
}

bool Intersection::isWithinInterval(unsigned int second, std::tuple<unsigned int, unsigned int> interval) {
	return second >= std::get<0>(interval) && second <= std::get<1>(interval) ? true : false;
}

void Intersection::connectAllLanes() {
	for (auto& road : roads) {
		for (unsigned int i = 0; i < road.lanes.size(); i++) {
			if (i > 0) {
				road.lanes[i].left = &road.lanes[i - 1];
			}
			if (i < road.lanes.size() - 1) {
				road.lanes[i].right = &road.lanes[i + 1];
			}
		}
	}
}
