#include "loader.h"

Intersection Loader::loadDefault() {
	Intersection intersection{};
	intersection.roads.emplace_back(loadRoadA());
	intersection.roads.emplace_back(loadRoadB());
	intersection.roads.emplace_back(loadRoadC());
	intersection.roads.emplace_back(loadRoadD());
	intersection.connectAllLanes();

	return intersection;
}

Road Loader::loadRoadA() {
	Road road{};
	road.directionChancePercent = { { Direction::straight, 25 }, { Direction::left, 7 }, { Direction::right, 25 } };
	road.createLanes(3);
	road.lanes[0].availableDirections = {
		{Direction::left, false},
		{Direction::straight, true},
		{Direction::right, true}
	};
	road.lanes[1].availableDirections = {
		{Direction::left, false},
		{Direction::straight, true},
		{Direction::right, false}
	};
	road.lanes[2].availableDirections = {
		{Direction::left, true},
		{Direction::straight, false},
		{Direction::right, false}
	};
	road.lanes[0].greenInterval = { 0, 39 };
	road.lanes[1].greenInterval = { 0, 39 };
	road.lanes[2].greenInterval = { 0, 10 };

	road.lanes[2].capped = true;
	road.lanes[2].capacity = 3;

	return road;
}

Road Loader::loadRoadB() {
	Road road{};
	road.directionChancePercent = { { Direction::straight, 5 }, { Direction::left, 0 }, { Direction::right, 0 } };
	road.createLanes(1);
	road.lanes[0].availableDirections = {
		{Direction::left, false},
		{Direction::straight, true},
		{Direction::right, false}
	};
	road.lanes[0].greenInterval = { 34, 55 };

	return road;
}

Road Loader::loadRoadC() {
	Road road{};
	road.directionChancePercent = { { Direction::straight, 20 }, { Direction::left, 0 }, { Direction::right, 17 } };
	road.createLanes(2);
	road.lanes[0].availableDirections = {
		{Direction::left, false},
		{Direction::straight, true},
		{Direction::right, true}
	};
	road.lanes[1].availableDirections = {
		{Direction::left, false},
		{Direction::straight, true},
		{Direction::right, false}
	};
	road.lanes[0].greenInterval = { 14, 39 };
	road.lanes[1].greenInterval = { 14, 39 };

	return road;
}

Road Loader::loadRoadD() {
	Road road{};
	road.directionChancePercent = { { Direction::straight, 7 }, { Direction::left, 0 }, { Direction::right, 0 } };
	road.createLanes(1);
	road.lanes[0].availableDirections = {
		{Direction::left, false},
		{Direction::straight, true},
		{Direction::right, false}
	};
	road.lanes[0].greenInterval = { 34, 55 };

	return road;
}
