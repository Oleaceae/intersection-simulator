#pragma once

#include <optional>
#include <map>
#include <tuple>

#include "car.h"

struct Lane {
	bool capped{ false };
	unsigned int capacity{};
	std::vector<std::optional<Car>> cars{};
	bool greenLight{ false };
	Lane* left{};
	Lane* right{};
	std::tuple<unsigned int, unsigned int> greenInterval{};
	std::map<Direction, bool> availableDirections{ {Direction::straight, false}, {Direction::left, false}, {Direction::right, false} };
};
